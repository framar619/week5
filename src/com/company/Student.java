package com.company;

public class Student {
    private String name;
    private String lastname;
    private int age;
    private int grade;


    public String getdisplayStudentName(){
        return this.name + this.lastname;
    }

    public int getAge() {
        return age;
    }
    public int getGrade(){
        return grade;
    }
    public Student(String name, String lastname, int age, int grade){
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.grade = grade;
}
    public String displayStudentName(String firstName, String lastName) {
        return firstName + lastName;
    }

    public static void main(String[] args) {
        Student student = new Student("Joe", "Brian", 12, 85);
        String displayName = student.getdisplayStudentName();

    }
}