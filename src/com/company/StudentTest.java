package com.company;

import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;
import static org.testng.AssertJUnit.assertFalse;

public class StudentTest {
    @Test

    public void testDisplayStudentName() {
        Student student = new Student("Anshuman", "Nain", 12,85);
        String studentName = student.displayStudentName("Anshuman", "Nain");
        assertEquals("Anshuman Nain", studentName);
    }

    @Test
    public void testAge() {
        Student student = new Student("Anshuman", "Nain", 18,85);
        assertEquals("+ 18: ", student.getAge(), 18);

    }


    public boolean validarMax(int maximo){
        boolean max = false;

        for (int i = 0; i < maximo; i++) {
            if(i == 85){
                max = true;
                break;
            }
        }

        return max;
    }

    public boolean validateMax(int valor, int max) {
        return valor >= max;
    }
    @Test
    public void testValidarMax() {
        Student student = new Student("Anshuman", "Nain", 18, 70);
        assertFalse("This grade is approved: ", validateMax(student.getGrade(), 85));
    }

}
